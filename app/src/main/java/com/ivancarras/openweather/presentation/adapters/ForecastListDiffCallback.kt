package com.ivancarras.openweather.presentation.adapters

import androidx.recyclerview.widget.DiffUtil
import com.ivancarras.openweather.domain.Forecast

/**
 * Created by Iván Carrasco Alonso on 13/04/2019.
 */
class ForecastListDiffCallback(private val newForecastLst: List<Forecast>, private val oldForecastLst: List<Forecast>) :
    DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldForecastLst[oldItemPosition].id == newForecastLst[newItemPosition].id

    override fun getOldListSize(): Int =
        oldForecastLst.size

    override fun getNewListSize(): Int =
        newForecastLst.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
        oldForecastLst[oldItemPosition].dt == newForecastLst[newItemPosition].dt
}