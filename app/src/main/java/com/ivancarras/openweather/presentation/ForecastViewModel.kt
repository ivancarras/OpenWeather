package com.ivancarras.openweather.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.ivancarras.openweather.domain.Forecast
import com.ivancarras.openweather.usecases.GetForecast
import io.reactivex.disposables.Disposable

/**
 * Created by Iván Carrasco Alonso on 12/04/2019.
 */
class ForecastViewModel(private val getForecast: GetForecast) : ViewModel() {
    private val forecast: MutableLiveData<List<Forecast>> by lazy {
        MutableLiveData<List<Forecast>>()
    }

    lateinit var disposable: Disposable

    fun getForecast(): LiveData<List<Forecast>> {
        return forecast.also {
            disposable = getForecast.invoke().doOnSuccess { list ->
                forecast.value = list
            }.subscribe()
        }
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}