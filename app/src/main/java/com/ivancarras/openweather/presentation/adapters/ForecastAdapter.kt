package com.ivancarras.openweather.presentation.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ivancarras.openweather.R
import com.ivancarras.openweather.domain.Forecast
import com.ivancarras.openweather.utils.extensions.addSufix
import com.ivancarras.openweather.utils.extensions.findView
import com.ivancarras.openweather.utils.extensions.format
import java.util.*
import kotlin.math.roundToInt


/**
 * Created by Iván Carrasco Alonso on 13/04/2019.
 */
class ForecastAdapter(val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val forecastData = mutableListOf<Forecast>()

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> ForecastItemViewType.MAIN_ITEM.value
            else -> ForecastItemViewType.SECONDARY_ITEM.value
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            ForecastItemViewType.MAIN_ITEM.value -> {
                val mainItemView = inflater.inflate(R.layout.item_main_forecast, parent, false)
                MainForecastViewHolder(mainItemView)
            }
            else
            -> {
                val secondaryItemView = inflater.inflate(R.layout.item_secondary_forecast, parent, false)
                SecondaryForecastViewHolder(secondaryItemView)
            }
        }
    }

    override fun getItemCount(): Int = forecastData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemData = forecastData[position]
        when (holder.itemViewType) {
            ForecastItemViewType.MAIN_ITEM.value -> {
                with(holder as MainForecastViewHolder) {
                    tvTemp.text = itemData.main?.temp
                        ?.roundToInt()
                        .toString()
                        .addSufix(context, R.string.degresCentigrades)
                    tvTempMax.text = itemData.main?.temp_max
                        ?.roundToInt()
                        .toString()
                        .addSufix(context, R.string.degresCentigrades)
                    tvTempMin.text = itemData.main?.temp_min
                        ?.roundToInt()
                        .toString()
                        .addSufix(context, R.string.degresCentigrades)
                    tvDateTime.text = itemData.dt?.let {
                        Date(it * 1000).format(MAIN_FORECAST_DATE_FORMAT, Locale.getDefault())
                    }
                    tvWindSpeed.text = itemData.wind?.speed.toString().addSufix(context, R.string.metersPerSecond)
                    tvCloudsPercentage.text = itemData.clouds?.all.toString().addSufix(context, R.string.percent)
                    tvAtmospherePressure.text = itemData.main?.pressure.toString().addSufix(context, R.string.hpa)
                    tvWeatherDescription.text = itemData.weather?.let {
                        if (it.isNotEmpty()) {
                            it[0].description.capitalize()
                        } else {
                            ""
                        }
                    }
                    itemData.weatherIcon?.let { bitmap ->
                        ivWeatherIcon.setImageBitmap(
                            BitmapFactory.decodeByteArray(itemData.weatherIcon, 0, bitmap.size)
                        )
                    }
                }

            }
            ForecastItemViewType.SECONDARY_ITEM.value -> {
                with(holder as SecondaryForecastViewHolder) {

                    tvTemp.text = itemData.main?.temp
                        ?.roundToInt()
                        .toString()
                        .addSufix(context, R.string.degresCentigrades)
                    tvTempMax.text = itemData.main?.temp_max
                        ?.roundToInt()
                        .toString()
                        .addSufix(context, R.string.degresCentigrades)
                    tvTempMin.text = itemData.main?.temp_min
                        ?.roundToInt()
                        .toString()
                        .addSufix(context, R.string.degresCentigrades)
                    tvDateTime.text = itemData.dt?.let {
                        Date(it * 1000).format(SECONDARY_FORECAST_DATE_FORMAT, Locale.getDefault())
                    }
                    tvWindSpeed.text = itemData.wind?.speed
                        .toString()
                        .addSufix(context, R.string.metersPerSecond)
                    tvCloudsPercentage.text = itemData.clouds?.all
                        .toString()
                        .addSufix(context, R.string.percent)
                    tvAtmospherePressure.text = itemData.main?.pressure
                        .toString()
                        .addSufix(context, R.string.hpa)
                    tvWeatherDescription.text = itemData.weather?.let {
                        if (it.isNotEmpty()) {
                            it[0].description.capitalize()
                        } else {
                            ""
                        }
                    }
                    itemData.weatherIcon?.let { bitmap ->
                        ivWeatherIcon.setImageBitmap(
                            BitmapFactory.decodeByteArray(itemData.weatherIcon, 0, bitmap.size)
                        )
                    }

                }
            }
        }
    }

    fun onForecastData(forecastData: List<Forecast>) {
        val diffResult = DiffUtil.calculateDiff(ForecastListDiffCallback(forecastData, this.forecastData))
        this.forecastData.clear()
        this.forecastData.addAll(forecastData)
        diffResult.dispatchUpdatesTo(this)
    }

    class MainForecastViewHolder(mainItemView: View) : RecyclerView.ViewHolder(mainItemView) {
        val tvTemp: TextView by mainItemView.findView(R.id.tvTemp)
        val tvTempMin: TextView by mainItemView.findView(R.id.tvTempMin)
        val tvTempMax: TextView by mainItemView.findView(R.id.tvTempMax)
        val tvDateTime: TextView by mainItemView.findView(R.id.tvDateTime)
        val tvWindSpeed: TextView by mainItemView.findView(R.id.tvWindSpeed)
        val tvCloudsPercentage: TextView by mainItemView.findView(R.id.tvCloudsPercentage)
        val tvAtmospherePressure: TextView by mainItemView.findView(R.id.tvAtmospherePressure)
        val tvWeatherDescription: TextView by mainItemView.findView(R.id.tvWeatherDescription)
        val ivWeatherIcon: ImageView by mainItemView.findView(R.id.ivWeatherIcon)
    }

    class SecondaryForecastViewHolder(secondaryItemView: View) : RecyclerView.ViewHolder(secondaryItemView) {
        val tvTemp: TextView by secondaryItemView.findView(R.id.tvTemp)
        val tvTempMin: TextView by secondaryItemView.findView(R.id.tvTempMin)
        val tvTempMax: TextView by secondaryItemView.findView(R.id.tvTempMax)
        val tvDateTime: TextView by secondaryItemView.findView(R.id.tvDateTime)
        val tvWindSpeed: TextView by secondaryItemView.findView(R.id.tvWindSpeed)
        val tvCloudsPercentage: TextView by secondaryItemView.findView(R.id.tvCloudsPercentage)
        val tvAtmospherePressure: TextView by secondaryItemView.findView(R.id.tvAtmospherePressure)
        val tvWeatherDescription: TextView by secondaryItemView.findView(R.id.tvWeatherDescription)
        val ivWeatherIcon: ImageView by secondaryItemView.findView(R.id.ivWeatherIcon)

    }

    enum class ForecastItemViewType(val value: Int) {
        MAIN_ITEM(1), SECONDARY_ITEM(2);
    }

    companion object {
        const val MAIN_FORECAST_DATE_FORMAT = "HH:MM - EEE d MMMM"
        const val SECONDARY_FORECAST_DATE_FORMAT = "HH:MM - EEE d MMMM"
    }
}