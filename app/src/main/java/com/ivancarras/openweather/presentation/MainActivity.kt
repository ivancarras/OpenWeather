package com.ivancarras.openweather.presentation

import android.media.Image
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ivancarras.openweather.R
import com.ivancarras.openweather.presentation.adapters.ForecastAdapter
import com.ivancarras.openweather.utils.extensions.findView
import org.koin.android.viewmodel.ext.android.viewModel

/**
 * Created by Iván Carrasco Alonso on 12/04/2019.
 */
class MainActivity : AppCompatActivity() {
    private val forecastViewModel: ForecastViewModel by viewModel()

    private val rvForecast: RecyclerView by findView(R.id.rvForecast)
    private val pbForecastLoad: ProgressBar by findView(R.id.pbForecastLoad)
    private val ivNoInternet: ImageView by findView(R.id.ivNoInternet)
    private val forecastAdapter = ForecastAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        rvForecast.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = forecastAdapter

        }
        forecastViewModel.getForecast()
            .observe(this, Observer {
                pbForecastLoad.visibility = View.GONE
                if (it.isNotEmpty()) {
                    forecastAdapter.onForecastData(it)
                } else {
                    ivNoInternet.visibility = View.VISIBLE
                }
            })
    }
}