package com.ivancarras.openweather.domain

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */

data class Main(
    val temp: Float, val pressure: Float, val humidity: Float, val temp_min: Float, val temp_max: Float,
    val sea_level: Float, val grnd_level: Float
)
