package com.ivancarras.openweather.domain

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.ivancarras.openweather.framework.room.Converters

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */
@Entity
@TypeConverters(Converters::class)
data class Forecast(
    @PrimaryKey
    val id: Int?,
    val coord: Coord?,
    val weather: List<Weather>?,
    val base: String?,
    val main: Main?,
    val wind: Wind?,
    val clouds: Clouds?,
    val rain: Rain?,
    val snow: Snow?,
    val dt: Long?,
    val sys: Sys?,
    val name: String?,
    val cod: Int?,
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    var weatherIcon: ByteArray?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Forecast

        if (weatherIcon != null) {
            if (other.weatherIcon == null) return false
            other.weatherIcon?.let {
                if (false == weatherIcon?.contentEquals(it)) return false
            }
        } else if (other.weatherIcon != null) return false

        return true
    }

    override fun hashCode(): Int {
        return weatherIcon?.contentHashCode() ?: 0
    }
}