package com.ivancarras.openweather.domain

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */

data class Sys(val type: Int, val id: Int, val message: Float, val country: String, val sunrise: Int, val sunset: Int)
