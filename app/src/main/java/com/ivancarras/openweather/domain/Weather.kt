package com.ivancarras.openweather.domain

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */

data class Weather(val id: Int, val main: String, val description: String, val icon: String)