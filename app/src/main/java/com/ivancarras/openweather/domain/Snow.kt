package com.ivancarras.openweather.domain

import com.google.gson.annotations.SerializedName

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */

data class Snow(@SerializedName("1h") val oneHour:Float, @SerializedName("3h")  val threeHours: Float)