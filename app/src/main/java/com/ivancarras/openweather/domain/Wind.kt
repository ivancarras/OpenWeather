package com.ivancarras.openweather.domain

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */

data class Wind(val speed: Float, val deg: Float)