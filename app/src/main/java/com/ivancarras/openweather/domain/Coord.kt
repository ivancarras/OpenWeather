package com.ivancarras.openweather.domain

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */

data class Coord(val lon:Float,val lat: Float)