package com.ivancarras.openweather.framework.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ivancarras.openweather.domain.Forecast

/**
 * Created by Iván Carrasco Alonso on 02/04/2019.
 */

@Database(entities = arrayOf(Forecast::class), version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun openWeatherDAO(): OpenWeatherDAO
}