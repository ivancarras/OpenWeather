package com.ivancarras.openweather.framework.DI

import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import com.ivancarras.openweather.data.*
import com.ivancarras.openweather.framework.ImForecastApiSource
import com.ivancarras.openweather.framework.ImForecastCacheManager
import com.ivancarras.openweather.framework.ImForecastIconsSource
import com.ivancarras.openweather.framework.ImForecastPersistenceSource
import com.ivancarras.openweather.framework.room.AppDatabase
import com.ivancarras.openweather.presentation.ForecastViewModel
import com.ivancarras.openweather.usecases.GetForecast
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Iván Carrasco Alonso on 12/04/2019.
 */
val frameworkModule = module {
    single {
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "forecast-db")
            .build().openWeatherDAO()
    }
    single {
        PreferenceManager.getDefaultSharedPreferences(androidApplication()) as SharedPreferences
    }
}

val presentationModule = module {
    viewModel { ForecastViewModel(get()) }
}

val usecasesModule = module {
    single {
        GetForecast(get())
    }
}

val dataModule = module {
    single {
        ForecastRepository(get(), get(), get(), get())
    }
    single {
        ImForecastApiSource() as ForecastApiSource
    }
    single {
        ImForecastPersistenceSource(get()) as ForecastPersistenceSource
    }

    single {
        ImForecastCacheManager(get()) as ForecastCacheManager
    }

    single {
        ImForecastIconsSource() as ForecastIconsApiSource
    }
}