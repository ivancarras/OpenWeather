package com.ivancarras.openweather.framework

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.ivancarras.openweather.data.ForecastApiSource
import com.ivancarras.openweather.domain.Forecast
import com.ivancarras.openweather.framework.api.ForecastApiInterface
import com.ivancarras.openweather.framework.api.ForecastCallResponse
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */
class ImForecastApiSource : ForecastApiSource {
    private val gson by lazy {
        GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    private val restClient by lazy {
        retrofit.create(ForecastApiInterface::class.java)
    }

    override fun getForecast(): Single<List<Forecast>> {

        val call = restClient.getForecastByCityName(
            API_KEY,
            API_PARAMETER_CITY_NAME,
            API_PARAMETER_UNITS,
            Locale.getDefault().language,
            API_PARAMETER_CNT
        )
        return Single.create<List<Forecast>> { emitter ->
            call.enqueue(object : retrofit2.Callback<ForecastCallResponse> {
                override fun onFailure(call: Call<ForecastCallResponse>, t: Throwable) {
                    emitter.onError(t)
                }

                override fun onResponse(call: Call<ForecastCallResponse>, response: Response<ForecastCallResponse>) {
                    response.body()?.let {
                        emitter.onSuccess(it.list)
                    } ?: emitter.onError(Throwable("Null response"))
                }
            })
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    companion object {
        const val API_BASE_URL = "https://api.openweathermap.org/data/2.5/"
        const val API_KEY = "07b4328c69ba55bf2e6e8a1a4b3b304f"
        const val API_PARAMETER_CITY_NAME = "Madrid"
        const val API_PARAMETER_UNITS = "metric"
        const val API_PARAMETER_CNT = 10
    }
}