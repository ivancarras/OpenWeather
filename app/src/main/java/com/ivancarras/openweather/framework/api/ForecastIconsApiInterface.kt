package com.ivancarras.openweather.framework.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Iván Carrasco Alonso on 14/04/2019.
 */
interface ForecastIconsApiInterface {
    @GET("{iconId}.png")
    fun getIcon(@Path("iconId") iconId: String): Call<ResponseBody>
}