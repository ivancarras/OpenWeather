package com.ivancarras.openweather.framework.api

import com.ivancarras.openweather.domain.Forecast

/**
 * Created by Iván Carrasco Alonso on 13/04/2019.
 */
data class ForecastCallResponse(val cod:Int,val message: String,val cnt: Int, val list: List<Forecast>)