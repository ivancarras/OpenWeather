package com.ivancarras.openweather.framework

/**
 * Created by Iván Carrasco Alonso on 13/04/2019.
 */
enum class ForecastDataSource {
    DB, API
}