package com.ivancarras.openweather.framework.room

import androidx.room.*
import com.ivancarras.openweather.domain.Forecast

/**
 * Created by Iván Carrasco Alonso on 02/04/2019.
 */
@Dao
interface OpenWeatherDAO {
    @Query("DELETE FROM forecast")
    fun clearDB()

    @Query("SELECT * FROM forecast")
    fun getForecast(): List<Forecast>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertForecast(Forecast: List<Forecast>)
}