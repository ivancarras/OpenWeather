package com.ivancarras.openweather.framework.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */
interface ForecastApiInterface {
    @GET("forecast")
    fun getForecastByCityName(
        @Query("appid") appid: String,
        @Query("q") cityName: String,
        @Query("units") units: String,
        @Query("lang") lang: String,
        @Query("cnt") cnt: Int
    ): Call<ForecastCallResponse>
}