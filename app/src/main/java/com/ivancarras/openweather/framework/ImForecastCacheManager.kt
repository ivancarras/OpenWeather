package com.ivancarras.openweather.framework

import android.content.SharedPreferences
import com.ivancarras.openweather.data.ForecastCacheManager
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Created by Iván Carrasco Alonso on 13/04/2019.
 */
class ImForecastCacheManager(private val sharedPreferences: SharedPreferences) : ForecastCacheManager {
    private val currentTime
        get() = Calendar.getInstance().timeInMillis

    override fun getDataSource(): ForecastDataSource =
        when (val lastDBUpdate = sharedPreferences.getLong(SHARED_PREFERENCE_LAST_DB_UPDATE, NEVER_UPDATED)) {
            NEVER_UPDATED -> ForecastDataSource.API
            else -> determineSource(lastDBUpdate)
        }


    override fun onDBChange() {
        val editor = sharedPreferences.edit()
        editor.putLong(SHARED_PREFERENCE_LAST_DB_UPDATE, currentTime)
        editor.apply()
    }

    private fun determineSource(lastDBUpdate: Long): ForecastDataSource =
        if ((currentTime - lastDBUpdate) < TimeUnit.MINUTES.toMillis(MAX_CACHE_TIME_MIN)) {
            ForecastDataSource.DB
        } else {
            ForecastDataSource.API
        }


    companion object {
        const val NEVER_UPDATED = -1L
        const val SHARED_PREFERENCE_LAST_DB_UPDATE = "last_db_update"
        const val MAX_CACHE_TIME_MIN = 5L
    }
}