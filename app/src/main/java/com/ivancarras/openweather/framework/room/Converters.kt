package com.ivancarras.openweather.framework.room

import androidx.room.TypeConverter
import com.ivancarras.openweather.domain.*
import com.ivancarras.openweather.utils.extensions.toObj
import com.ivancarras.openweather.utils.helpers.objectToString

/**
 * Created by Iván Carrasco Alonso on 12/04/2019.
 */
class Converters {

    @TypeConverter
    fun coordToString(coord: Coord?): String? = objectToString(coord)

    @TypeConverter
    fun weatherLstToString(weather: List<Weather>?): String? = objectToString(weather)

    @TypeConverter
    fun mainToString(main: Main?): String? = objectToString(main)

    @TypeConverter
    fun snowToString(snow: Snow?): String? = objectToString(snow)

    @TypeConverter
    fun rainToString(rain: Rain?): String? = objectToString(rain)

    @TypeConverter
    fun windToString(wind: Wind?): String? = objectToString(wind)

    @TypeConverter
    fun sysToString(sys: Sys?): String? = objectToString(sys)

    @TypeConverter
    fun cloudsToString(clouds: Clouds?): String? = objectToString(clouds)

    @TypeConverter
    fun stringToCoord(coordJson: String?): Coord? = coordJson?.toObj()

    @TypeConverter
    fun stringToWeatherLst(weatherLstJson: String?): List<Weather>? = weatherLstJson?.toObj()

    @TypeConverter
    fun stringToMain(mainJson: String?): Main? = mainJson?.toObj()

    @TypeConverter
    fun stringToSnow(snowJSon: String?): Snow? = snowJSon?.toObj()

    @TypeConverter
    fun stringToRain(rainJson: String?): Rain? = rainJson?.toObj()

    @TypeConverter
    fun stringToWind(windJson: String?): Wind? = windJson?.toObj()

    @TypeConverter
    fun stringToSys(sysJson: String?): Sys? = sysJson?.toObj()

    @TypeConverter
    fun stringToClouds(cloudsJson: String?): Clouds? = cloudsJson?.toObj()

}