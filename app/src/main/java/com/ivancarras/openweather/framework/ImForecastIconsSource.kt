package com.ivancarras.openweather.framework

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.ivancarras.openweather.data.ForecastIconsApiSource
import com.ivancarras.openweather.domain.Forecast
import com.ivancarras.openweather.framework.api.ForecastIconsApiInterface
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.atomic.AtomicInteger


/**
 * Created by Iván Carrasco Alonso on 14/04/2019.
 */
class ImForecastIconsSource : ForecastIconsApiSource {
    private val gson by lazy {
        GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }
    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    private val restClient by lazy {
        retrofit.create(ForecastIconsApiInterface::class.java)
    }


    override fun saveIcons(forecastLst: List<Forecast>): Observable<Unit> =
        Observable.create<Unit> { emitter ->
            val emitted = AtomicInteger(0)
            forecastLst.forEach { item ->
                item.weather?.let {
                    if (it.isNotEmpty()) {
                        val call = restClient.getIcon(it[0].icon)
                        call.enqueue(object : retrofit2.Callback<ResponseBody> {
                            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                response.body()?.let {
                                    item.weatherIcon = it.bytes()

                                    if (emitted.incrementAndGet() < forecastLst.size) {
                                        emitter.onNext(Unit)
                                    } else {
                                        emitter.onComplete()
                                    }
                                } ?: emitter.onError(Throwable("Null response"))
                            }

                            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                emitter.onError(t)
                            }
                        })
                    }
                }
            }
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    companion object {
        const val API_BASE_URL = "https://openweathermap.org/img/w/"
    }

}