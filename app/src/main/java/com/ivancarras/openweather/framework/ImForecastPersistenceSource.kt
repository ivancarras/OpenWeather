package com.ivancarras.openweather.framework

import com.ivancarras.openweather.data.ForecastPersistenceSource
import com.ivancarras.openweather.domain.Forecast
import com.ivancarras.openweather.framework.room.OpenWeatherDAO
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */
class ImForecastPersistenceSource(private val openWeatherDAO: OpenWeatherDAO) : ForecastPersistenceSource {
    override fun getSavedForecast(): Single<List<Forecast>> =
        Single.fromCallable {
            openWeatherDAO.getForecast()
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())


    override fun saveForecast(forecastLst: List<Forecast>): Single<Unit> =
        Single.fromCallable {
            openWeatherDAO.clearDB()
            openWeatherDAO.insertForecast(forecastLst)
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

}