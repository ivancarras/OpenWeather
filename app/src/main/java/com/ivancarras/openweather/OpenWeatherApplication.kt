package com.ivancarras.openweather

import android.app.Application
import com.ivancarras.openweather.framework.DI.dataModule
import com.ivancarras.openweather.framework.DI.frameworkModule
import com.ivancarras.openweather.framework.DI.presentationModule
import com.ivancarras.openweather.framework.DI.usecasesModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


/**
 * Created by Iván Carrasco Alonso on 12/04/2019.
 */
class OpenWeatherApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            this.androidContext(this@OpenWeatherApplication)
            modules(frameworkModule, dataModule, usecasesModule, presentationModule)
        }
    }
}