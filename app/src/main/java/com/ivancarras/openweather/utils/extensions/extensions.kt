package com.ivancarras.openweather.utils.extensions

import android.app.Activity
import android.content.Context
import android.view.View
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Iván Carrasco Alonso on 12/04/2019.
 */

inline fun <reified T> String.toObj(): T =
    Gson().fromJson(this, object : TypeToken<T>() {}.type)

fun String.addSufix(context: Context, suffixR: Int): String {
    val suffixString = context.resources.getString(suffixR)
    return this + suffixString
}

fun <T : View> Activity.findView(id: Int): Lazy<T> {
    return lazy {
        findViewById<T>(id)
    }
}

fun <T : View> View.findView(id: Int): Lazy<T> {
    return lazy {
        findViewById<T>(id)
    }
}

fun Date.format(pattern: String, locale: Locale = Locale.US): String? =
    try {
        SimpleDateFormat(pattern, locale).format(this)
    } catch (e: Throwable) {
        null
    }
