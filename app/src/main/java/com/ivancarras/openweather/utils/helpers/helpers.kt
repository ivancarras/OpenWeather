package com.ivancarras.openweather.utils.helpers

import com.google.gson.Gson

/**
 * Created by Iván Carrasco Alonso on 13/04/2019.
 */
fun objectToString(obj: Any?): String =
    Gson().toJson(obj)