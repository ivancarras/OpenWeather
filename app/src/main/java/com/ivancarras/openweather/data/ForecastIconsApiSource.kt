package com.ivancarras.openweather.data

import com.ivancarras.openweather.domain.Forecast
import io.reactivex.Observable

/**
 * Created by Iván Carrasco Alonso on 14/04/2019.
 */
interface ForecastIconsApiSource {
    fun saveIcons(forecastLst: List<Forecast>): Observable<Unit>
}