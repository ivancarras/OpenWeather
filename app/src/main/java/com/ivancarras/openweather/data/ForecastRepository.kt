package com.ivancarras.openweather.data

import com.ivancarras.openweather.domain.Forecast
import com.ivancarras.openweather.framework.ForecastDataSource
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */
class ForecastRepository(
    private val forecastPersistenceSource: ForecastPersistenceSource,
    private val forecastApiSource: ForecastApiSource,
    private val forecastCacheManager: ForecastCacheManager,
    private val forecastIconsSource: ForecastIconsApiSource
) {
    /*
       We Have to define the cache logic, the aim is, this repository use de API call when the current time is bigger
       than the difference betweeen the time that you saved the info in database and the current time
     */
    fun getForecast(): Single<List<Forecast>> {
        return Single.create<List<Forecast>> { mainEmitter ->
            when (forecastCacheManager.getDataSource()) {
                ForecastDataSource.API -> {
                    forecastApiSource.getForecast().subscribe { list, error ->
                        error?.let {
                            mainEmitter.onError(error)
                        } ?: saveWeatherIcons(list, mainEmitter)
                    }
                }

                ForecastDataSource.DB -> {
                    forecastPersistenceSource.getSavedForecast().subscribe { list, error ->
                        error?.let {
                            mainEmitter.onError(it)
                        } ?: mainEmitter.onSuccess(list)
                    }
                }
            }

        }
            .onErrorReturn { emptyList() }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    private fun saveWeatherIcons(forecastLst: List<Forecast>, mainEmitter: SingleEmitter<List<Forecast>>) {
        forecastIconsSource.saveIcons(forecastLst)
            .doOnComplete {
                forecastPersistenceSource.saveForecast(forecastLst).subscribe { _, error ->
                    error?.let {
                        mainEmitter.onError(it)
                    } ?: forecastCacheManager.onDBChange()
                    mainEmitter.onSuccess(forecastLst)
                }
            }.doOnError {
                mainEmitter.onError(it)
            }.subscribe()
    }
}
