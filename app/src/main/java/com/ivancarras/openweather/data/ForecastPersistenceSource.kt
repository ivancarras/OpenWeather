package com.ivancarras.openweather.data

import com.ivancarras.openweather.domain.Forecast
import io.reactivex.Single

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */
interface ForecastPersistenceSource {
    fun getSavedForecast(): Single<List<Forecast>>
    fun saveForecast(forecastLst: List<Forecast>): Single<Unit>
}