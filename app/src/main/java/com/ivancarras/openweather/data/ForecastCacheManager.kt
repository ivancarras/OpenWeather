package com.ivancarras.openweather.data

import com.ivancarras.openweather.framework.ForecastDataSource

/**
 * Created by Iván Carrasco Alonso on 13/04/2019.
 */
interface ForecastCacheManager {
    fun getDataSource(): ForecastDataSource
    fun onDBChange()
}