package com.ivancarras.openweather.usecases

import com.ivancarras.openweather.data.ForecastRepository
import com.ivancarras.openweather.domain.Forecast
import io.reactivex.Single

/**
 * Created by Iván Carrasco Alonso on 11/04/2019.
 */
class GetForecast(private val forecastRepository: ForecastRepository) {
    operator fun invoke(): Single<List<Forecast>> = forecastRepository.getForecast()
}